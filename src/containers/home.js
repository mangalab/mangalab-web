import React, { Component } from 'react';
// import { NavLink } from 'react-router-dom';

import HomeTop from 'components/home/hometop.js';
import HomeBottom from 'components/home/homebottom.js';

import 'css/Home.css';

export default class Home extends Component {
    render() {
        return (
            <div id='Home'>
                <div className='HomeInner'>
                    <HomeTop />
                    <HomeBottom />
                </div>
            </div>
        )
    }
}