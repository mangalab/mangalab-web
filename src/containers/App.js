import React, { Component } from 'react';
import Header from 'components/header';
import { Route } from 'react-router';
import Home from 'containers/home.js'

import 'css/App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
        {/* Add Header */}
        <Header />
        </header>
        {/* Routes for body */}
        <Route exact path="/" component={Home} />
        
      </div>
    );
  }
}

export default App;
