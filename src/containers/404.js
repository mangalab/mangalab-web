import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import NotFoundImage from 'assets/images/404.svg';

import 'css/404.css';

export default class NotFound extends Component {
    render() {
        const RandomQoute = [
            'Hey Major Tom, looks like you’re on the wrong planet!',
            'Grats. You broke it.',
            'You look lost...',
            'You seem to have stumbled off the beaten path',
            'Whoops! What are you doing here?'
        ];

        const max = RandomQoute.length;

        return (
            <div>
                <div className="NotFoundContainer">
                    <img alt="404" src={NotFoundImage} />
                    <p>{RandomQoute[Math.floor(Math.random() * Math.floor(max))]}</p>
                    <NavLink to="/">
                        <button className="button brand default large">Go Back to Manga Land!</button>
                    </NavLink>
                </div>
            </div>
        )
    }
}