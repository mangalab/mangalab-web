import React, { Component } from 'react';

import homebottomimg from 'assets/images/homebottom.png';

export default class HomeBottom extends Component {
    render() {
        return (
            <div className="HomeBottom">
                <div className="HomeManga">
                    <img src={homebottomimg} alt="discord" />
                </div>
            </div>
        );
    }
}