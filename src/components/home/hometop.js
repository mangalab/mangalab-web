import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';


import lab from 'assets/images/lab.svg';

export default class HomeTop extends Component {
    render() {
        return (
            <div className='HomeTop'>
                <div className='textWrapper'>
                    <h1>Organise Your Manga!</h1>
                    <p>ML is a manga library organiser to keep you up-to-date with the latest manga!</p>
                </div>
                <div className='buttonsWrapper'>
                    <div className="buttonWrapper">
                        <NavLink to="/downloads">
                            <button type="button" className="button brand default large grow ">
                                <img src={lab} alt="discord" />
                                Downloads
                                    </button>
                        </NavLink>
                    </div>
                    <div className="buttonWrapper">
                        <NavLink to="/downloads">
                            <button type="button" className="button black default large grow">
                                Learn More
                                    </button>
                        </NavLink>
                    </div>
                </div>
            </div>
        )
    }
}