import React, { PureComponent } from 'react';
import HeadMenu from 'components/header/headermenu';
import HeaderLogin from 'components/header/headerlogin';
import { NavLink } from 'react-router-dom';

import logo from 'assets/images/logo.png';
import 'css/Header.css';

const Logo = ({ src, onClick, alt }) => (
    <NavLink className="logo-image" to="/" onClick={onClick}>
        <img src={src} alt={alt} className="adventureLogo"/>
        MangaLab
    </NavLink>
)


export default class header extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            mobileMenuIsActive: false,
            burgerStatus: false,
            width: 0
        }
    }

    updateDimensions() {
        if(this.state.mobileMenuIsActive === true && window.innerWidth >= 645) {
            this.setState({mobileMenuIsActive: false});
        }
        if(this.state.mobileMenuIsActive === false && window.innerWidth <= 645) {
            this.setState({mobileMenuIsActive: true});
        }
    }

    toggleBurger() {
        console.log(this.state.burgerToggle);
        this.setState({burgerStatus: !this.state.burgerStatus});
    }

    componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
      }

      componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
      }

    render() {

        if(this.state.mobileMenuIsActive === false) {
            return (
                <div className="desktopHeader headerTransparent">
                    <div className="desktopHeaderInner">
                        <div className="desktopHeaderLogo">
                        <Logo src={logo} onClick={this.closeAll} alt="logo" />
                        </div>
                        {/* Left Nav */}
                        <HeadMenu mobileMenuIsActive={this.state.mobileMenuIsActive} burgerStatus/>
                        {/* Right Nav */}
                        {console.log(window.innerWidth)}
                        <HeaderLogin />
                    </div>
                </div>
            )
        }
        else {
            return (
                <div className={`mobileHeader ${this.state.burgerStatus ? "headerBlack" : "headerTransparent"}`}>
                    <div className="mobileHeaderInner">
                        <button className={`burgerToggle ${this.state.burgerStatus ? "active" : ''}`} onClick={this.toggleBurger.bind(this)}>
                            <span className='burgerIcon' ></span>
                        </button>
                        <div className='mobileHeaderLogo'>
                        <NavLink className="logo-image" to="/">
                        MANGALAB
                        </NavLink>
                        <HeadMenu mobileMenuIsActive={this.state.mobileMenuIsActive} burgerStatus={this.state.burgerStatus}/>
                        </div>
                        {/* Right Nav */}
                        {console.log(window.innerWidth)}
                        <HeaderLogin mobileMenuIsActive={this.state.mobileMenuIsActive}/>
                    </div>
                </div>
            )
        }
        
    }
}