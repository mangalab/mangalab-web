import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

export default class HeadMenu extends Component {
    render() {
        let { mobileMenuIsActive, burgerStatus} = this.props;
        if (mobileMenuIsActive === false) {
            return (
                <ul className='desktopHeaderLeftNav'>
                    <li className='desktopHeaderLeftNavItem'>
                        <NavLink to="/feature">Feature</NavLink>
                    </li>
                    <li className='desktopHeaderLeftNavItem'>
                        <NavLink to="/blog">Blog</NavLink>
                    </li>
                    <li className='desktopHeaderLeftNavItem'>
                        <NavLink to="/community">Community</NavLink>
                    </li>
                </ul>
            )
        }
        else {
            return (
                <ul className={`headerBlack mobileHeaderLeftNav ${burgerStatus ? "open" : ""}`}>
                    <li className='mobileHeaderLeftNavItem'>
                        <NavLink to="/feature">Features</NavLink>
                    </li>
                    <li className='mobileHeaderLeftNavItem'>
                        <NavLink to="/blog">Blog</NavLink>
                    </li>
                    <li className='mobileHeaderLeftNavItem'>
                        <NavLink to="/community">Community</NavLink>
                    </li>
                </ul>
            )
        }
        
    }
}