import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import reducers from 'reducers';

import App from 'containers/App';
import NotFound from 'containers/404';


import './index.css';

import registerServiceWorker from './registerServiceWorker';

const store = createStore(reducers, applyMiddleware());

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={App} />
                <Route path="*" component={NotFound} />
            </Switch>
        </BrowserRouter>
    </Provider>,
    document.getElementById('app'));

registerServiceWorker();
